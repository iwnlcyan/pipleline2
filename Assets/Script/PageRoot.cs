using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

public class PageRoot : MonoBehaviour
{
    private enum PageState
    {
        Hidden = 0, Show = 1, Shown = 2, Hide = 3
    }

    private static readonly int STATE_KEY = Animator.StringToHash(@"State");

    [SerializeField] private Animator _pageAnimator;

    public void Show()
    {
        _pageAnimator.SetInteger(STATE_KEY, (int)PageState.Show);
    }

    public void Hide()
    {
        _pageAnimator.SetInteger(STATE_KEY, (int)PageState.Hide);
    }
}
