using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using TMPro;

using static SkillData;

public class SelectingPage : MonoBehaviour
{
    private enum SelectingPageState
    {
        Default = 0, Detail = 1
    }

    private static readonly int STATE_KEY = Animator.StringToHash(@"State");

    [SerializeField] private PageRoot _pageRoot;

    [SerializeField] private SelectingItem _selectingItemPrefab;
    [SerializeField] private Transform _selectingItemsRoot;
    [SerializeField] private Button _toNextPhaseBtn;
    [SerializeField] private Animator _animator;

    [Header("Detail View")]
    [SerializeField] private TextMeshProUGUI _detailTitle;
    [SerializeField] private TextMeshProUGUI _detailDescription;
    [SerializeField] private Button _detailBackBtn;

    private List<SelectingItem> _selectingItems = new List<SelectingItem>();

    public void Init()
    {
        _animator.SetInteger(STATE_KEY, (int)SelectingPageState.Default);

        _detailBackBtn.onClick.AddListener(_CloseDetailView);

        _SetupSelectingItems();
    }

    public async UniTask<List<SkillType>> Flow()
    {
        _pageRoot.Show();

        _SetState(SelectingPageState.Default);

        // completed
        await _toNextPhaseBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken());

        _pageRoot.Hide();

        return _CollectSelectedSkillType();
    }

    private List<SkillType> _CollectSelectedSkillType()
    {
        var selectedSkillType = new List<SkillType>();

        foreach (var item in _selectingItems)
        {
            if (item.State == SelectingItem.SelectingItemState.Selected)
            {
                selectedSkillType.Add(item.SkillType);
            }
        }

        return selectedSkillType;
    }

    private void _SetupSelectingItems()
    {
        _ClearSelectingItems();

        var skillData = SkillData.MockData;

        foreach (var skill in skillData)
        {
            var skillItem = Instantiate(_selectingItemPrefab, _selectingItemsRoot);

            skillItem.Init(skill.Key, skill.Value.Title, onDetailBtnClicked: () =>
            {
                _ShowDetailView(skill.Value);
            });

            _selectingItems.Add(skillItem);
        }
    }

    private void _ShowDetailView(Skill skill)
    {
        _SetState(SelectingPageState.Detail);

        _detailTitle.text = skill.Title;
        _detailDescription.text = skill.Description;

        // TODO: radar chart
    }

    private void _CloseDetailView()
    {
        _SetState(SelectingPageState.Default);
    }

    private void _ClearSelectingItems()
    {
        foreach (var item in _selectingItems)
            Destroy(item.gameObject);

        _selectingItems.Clear();
    }

    private void _SetState(SelectingPageState state)
    {
        _animator.SetInteger(STATE_KEY, (int)state);
    }
}
