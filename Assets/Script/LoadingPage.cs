using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

public class LoadingPage : MonoBehaviour
{
    private enum LoadingPageState
    {
        Default = 0, Loading = 1, Completed = 2
    }

    private static readonly int STATE_KEY = Animator.StringToHash(@"State");

    [SerializeField] private PageRoot _pageRoot;

    [SerializeField] private Button _toNextPhaseBtn;
    [SerializeField] private Animator _animator;

    public void Init()
    {
        _SetState(LoadingPageState.Default);
    }

    public async UniTask Flow()
    {
        _pageRoot.Show();

        await UniTask.Delay(1000);

        // DNA loading
        _SetState(LoadingPageState.Loading);

        // completed
        await _toNextPhaseBtn.onClick.OnInvokeAsync(new System.Threading.CancellationToken());

        _pageRoot.Hide();
    }

    private void _SetState(LoadingPageState state)
    {
        _animator.SetInteger(STATE_KEY, (int)state);
    }
}
